package bcccp.carpark;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bcccp.carpark.Carpark;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.season.ISeasonTicketDAO;

public class TestCarpark {
	
	Carpark sut;
	
	IAdhocTicketDAO adhocDao;
	ISeasonTicketDAO seasonDao;
	
	String name;
	int capacity;
	int seasonTicketSpaces;
	
	
	@Before
	public void setUp(){
		name = "testName";
		capacity = 5;
		seasonTicketSpaces = 2;
		adhocDao = mock(IAdhocTicketDAO.class);
		seasonDao = mock(ISeasonTicketDAO.class);
		
		sut = new Carpark(name,capacity,adhocDao,seasonDao);
		
	}
	
	@After
	public void breakDown(){
		sut = null;
	}
	
	
	/**
	 * Unique name for carpark
	 * Specify total capacity
	 * Specify capacity for season tickets
	 * 
	 */
	@Test
	public void testConstructor(){
		assertTrue(sut instanceof Carpark);
		assertTrue(sut.getName() instanceof String);
		assertTrue(Integer.class.isInstance(sut.getCapacity()) );
		assertTrue(sut.getAdhocTicketDAO() instanceof IAdhocTicketDAO);
		assertTrue(sut.getSeasonTicketDAO() instanceof ISeasonTicketDAO);
		
		assertEquals(sut.getName(), this.name);
		assertEquals(sut.getCapacity(), this.capacity);
		assertEquals(sut.getAdhocTicketDAO(), this.adhocDao);
		assertEquals(sut.getSeasonTicketDAO(), this.seasonDao);
	
	}

}

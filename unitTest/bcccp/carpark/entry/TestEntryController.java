package bcccp.carpark.entry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bcccp.carpark.ICarSensor;
import bcccp.carpark.ICarSensorResponder;
import bcccp.carpark.ICarpark;
import bcccp.carpark.ICarparkObserver;
import bcccp.carpark.IGate;
import bcccp.carpark.entry.EntryController.STATE;
import bcccp.tickets.adhoc.IAdhocTicket;

public class TestEntryController {
	
	EntryController sut;
	
	ICarpark carpark;
	ICarSensor outsideEntrySensor;
	ICarSensor insideEntrySensor;
	IEntryUI entryUI;
	IGate entryGate;
	IAdhocTicket adhoc;
	
	bcccp.carpark.entry.EntryController.STATE status;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		carpark = mock(ICarpark.class);
		outsideEntrySensor = mock(ICarSensor.class);
		insideEntrySensor = mock(ICarSensor.class);
		entryGate = mock(IGate.class);
		entryUI = mock(IEntryUI.class);
		adhoc = mock(IAdhocTicket.class);
		
		sut = new EntryController(carpark, entryGate, 
				outsideEntrySensor, insideEntrySensor, entryUI);
	}

	@After
	public void tearDown() throws Exception {
		sut = null;
	}

	@Test
	public void testEntryController() {
		
		verify(outsideEntrySensor).registerResponder(sut);
		verify(insideEntrySensor).registerResponder(sut);
		verify(entryUI).registerController(sut);
		
		assertEquals(sut.getState(),STATE.IDLE);
		assertTrue(sut instanceof IEntryController);
		assertTrue(sut instanceof ICarSensorResponder);
		assertTrue(sut instanceof ICarparkObserver);
		
	}
	
	@Test(expected=RuntimeException.class) 
	public void testEntryControllerEmptyCarpark() throws Exception {
		sut = new EntryController(null, entryGate, 
				outsideEntrySensor, insideEntrySensor, entryUI);	
		fail("Should bave thrown exception");
	}
	
	@Test(expected=RuntimeException.class) 
	public void testEntryControllerEmptyEntryGate() throws Exception {
		sut = new EntryController(carpark, null, 
				outsideEntrySensor, insideEntrySensor, entryUI);	
		fail("Should bave thrown exception");
	}
	
	@Test(expected=RuntimeException.class) 
	public void testEntryControllerEmptyOutsideSensor() throws Exception {
		sut = new EntryController(carpark, entryGate, 
				null, insideEntrySensor, entryUI);	
		fail("Should bave thrown exception");
	}
	
	@Test(expected=RuntimeException.class) 
	public void testEntryControllerEmptyInsideSensor() throws Exception {
		sut = new EntryController(carpark, entryGate, 
				outsideEntrySensor, null, entryUI);	
		fail("Should bave thrown exception");
	}
	
	@Test(expected=RuntimeException.class) 
	public void testEntryControllerEmptyEntryUI() throws Exception {
		sut = new EntryController(carpark, entryGate, 
				outsideEntrySensor, insideEntrySensor, null);	
		fail("Should bave thrown exception");
	}

	@Test
	public void testCarEventDetectedStateIdle() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.IDLE);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.WAITING,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateWaiting() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.WAITING);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateFull() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.FULL);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateIssued() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.ISSUED);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateValidated() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.VALIDATED);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateWaitingInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.WAITING);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.BLOCKED,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateFullInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.FULL);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.BLOCKED,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateIssuedInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.ISSUED);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.BLOCKED,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateValidatedInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.VALIDATED);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.BLOCKED,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateBlocked() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.BLOCKED);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateBlockedInside() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.IDLE);
		sut.setState(STATE.BLOCKED);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateTaken() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.TAKEN);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateTakenInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.TAKEN);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.ENTERING,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateEntering() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.ENTERING);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.ENTERED,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateEnteringInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.ENTERING);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.TAKEN,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateEntered() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.ENTERED);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.ENTERING,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateEnteredInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.ENTERED);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(entryGate).lower();
		verify(insideEntrySensor).getId();
		
	}

	@Test
	public void testButtonPushed() throws Exception {
		//setup
		when(carpark.isFull()).thenReturn(false);
		when(carpark.issueAdhocTicket()).thenReturn(adhoc);
		when(adhoc.getCarparkId()).thenReturn("ATestID");
		when(adhoc.getTicketNo()).thenReturn(123);
		when(adhoc.getBarcode()).thenReturn("ATestID123");
		long entryDateTime = LocalDateTime.of(2017, Month.AUGUST, 21,20,17).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
		when(adhoc.getEntryDateTime()).thenReturn(entryDateTime);
		sut.setState(STATE.WAITING);
		
		//exec
		sut.buttonPushed();
		
		//verifies and asserts
		verify(carpark).isFull();
		verify(carpark).issueAdhocTicket();
		verify(adhoc).getCarparkId();
		verify(adhoc).getTicketNo();
		verify(adhoc).getBarcode();
		verify(entryUI).printTicket("ATestID",123, entryDateTime, "ATestID123");
		
		assertEquals(STATE.ISSUED, sut.getState());
		assertEquals(entryDateTime, sut.getEntryTime());
		
	}
	
	@Test
	public void testButtonPushedFull() throws Exception {
		//setup
		when(carpark.isFull()).thenReturn(true);
		sut.setState(STATE.WAITING);
		
		//exec
		sut.buttonPushed();
		
		//verifies and asserts
		verify(carpark).isFull();
		verify(entryUI).display("CARPARK FULL");
		
		assertEquals(STATE.FULL, sut.getState());
		
	}
	
	@Test
	public void testButtonPushedOtherState() throws Exception {
		//setup
		when(carpark.isFull()).thenReturn(false);
		sut.setState(STATE.IDLE);
		
		//exec
		sut.buttonPushed();
		
		//verifies and asserts
		assertEquals(STATE.IDLE, sut.getState());
		verify(entryUI).beep();
		
	}

	@Test
	public void testTicketInserted() {
		//setup
		String testBarcode = "ATesID123";
		sut.setState(STATE.WAITING);
		when(carpark.isSeasonTicketValid(testBarcode)).thenReturn(true);
		when(carpark.isSeasonTicketInUse(testBarcode)).thenReturn(false);
		
		//exec
		sut.ticketInserted(testBarcode);
		
		//verifies and asserts
		verify(carpark).isSeasonTicketValid(testBarcode);
		verify(carpark).isSeasonTicketInUse(testBarcode);
		
		assertEquals(STATE.VALIDATED, sut.getState());
		assertEquals(testBarcode, sut.getSeasonTicketId());
		
	}

	@Test
	public void testTicketTakenStateIssued() {
		//setup
		sut.setState(STATE.ISSUED);
		
		//exec
		sut.ticketTaken();
		
		//verifies and asserts
		verify(entryUI).display("Ticket Taken");
		verify(entryGate).raise();
		assertEquals(STATE.TAKEN, sut.getState());
		
	}
	
	@Test
	public void testTicketTakenStateValidated() {
		//setup
		sut.setState(STATE.VALIDATED);
		
		//exec
		sut.ticketTaken();
		
		//verifies and asserts
		verify(entryUI).display("Ticket Taken");
		verify(entryGate).raise();
		assertEquals(STATE.TAKEN, sut.getState());
		
	}
	
	@Test
	public void testTicketTakenStateOther() {
		//setup
		sut.setState(STATE.IDLE);
		
		//exec
		sut.ticketTaken();
		
		//verifies and asserts
		verify(entryUI).beep();
		
	}

	@Test
	public void testNotifyCarparkEvent() {
		//setup
		sut.setState(STATE.FULL);
		when(carpark.isFull()).thenReturn(false);
		
		//exec
		sut.notifyCarparkEvent();
		
		//verifies and asserts
		verify(carpark).isFull();
		verify(entryUI).display("Push Button");
		
		assertEquals(STATE.WAITING, sut.getState());
	}

}

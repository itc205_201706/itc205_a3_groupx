package bcccp.tickets.adhoc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestAdhocTicket {
	
	IAdhocTicket sut;
	
	String carparkId;
	int ticketNo;
	String barcode;
	long enterTime;
	long paidTime;
	long exitTime;
	float charge;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		carparkId = "tCarpark";
		ticketNo = 123;
		barcode = "A123";
		sut = new AdhocTicket(carparkId, ticketNo, barcode);
	}

	@After
	public void tearDown() throws Exception {
		sut = null;
	}

	@Test
	public void testAdhocTicket() {
		
		assertTrue(sut instanceof IAdhocTicket);
		assertTrue(sut.isIssued());
		
		assertEquals(sut.getTicketNo(), ticketNo);
		assertEquals(sut.getBarcode(), barcode);
		assertEquals(sut.getCarparkId(), carparkId);
		
	}
	
	@Test(expected=Exception.class) 
	public void testConstructorIdNumberLessThanZero() throws Exception {
		sut = new AdhocTicket(carparkId, -1, barcode);	
		fail("Should bave thrown exception");
	}
	
	@Test(expected=RuntimeException.class) 
	public void testConstructorEmptyCarpark() throws Exception {
		sut = new AdhocTicket(null, ticketNo, barcode);	
		fail("Should bave thrown exception");
	}
	
	@Test(expected=RuntimeException.class) 
	public void testConstructorEmptyBarcode() throws Exception {
		sut = new AdhocTicket(carparkId, ticketNo, null);	
		fail("Should bave thrown exception");
	}
	
	@Test
	public void testEnter(){
		//setup
		enterTime = LocalDateTime.of(2017, Month.SEPTEMBER, 17,15,32).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
		
		//exec
		sut.enter(enterTime);
		
		//verifies and assert
		assertEquals(sut.getEntryDateTime(), enterTime);
		assertTrue(sut.isCurrent());
		
	}
	
	@Test
	public void testPay(){
		//setup
		charge = 5.0f;
		paidTime = System.currentTimeMillis();
		
		//exec
		sut.pay(paidTime, charge);
		
		//verifies and assert
		assertEquals(sut.getPaidDateTime(),paidTime);
		assertEquals(sut.getCharge(), charge,0.001);
		assertTrue(sut.isPaid());
	}
	
	@Test
	public void testExit(){
		//setup
		exitTime = System.currentTimeMillis();
		
		//exec
		sut.exit(exitTime);
		
		//verifies and assert
		assertEquals(sut.getExitDateTime(), exitTime);
		assertTrue(sut.hasExited());
		
	}
	
	

}

package bcccp.tickets.adhoc;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestAdhocTicketFactory {
	String carparkId;
	int ticketNo;
	IAdhocTicketFactory sut;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		carparkId = "testCarpark";
		ticketNo = 123;
		
		sut = new AdhocTicketFactory();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMake() throws Exception {
		//setup
		
		
		//exec
		IAdhocTicket created = sut.make(carparkId, ticketNo);
		
		
		//verifies and asserts
		assertTrue(created instanceof IAdhocTicket);
		
		
	}
	
	@Test(expected=RuntimeException.class) 
	public void testMakeEmptyCarpark() throws Exception {
		IAdhocTicket created = sut.make(null, ticketNo);	
		fail("Should bave thrown exception");
	}
	
	@Test(expected=RuntimeException.class) 
	public void testMakeTicketNoLessThanZero() throws Exception {
		IAdhocTicket created = sut.make(carparkId, -1);	
		fail("Should bave thrown exception");
	}

}

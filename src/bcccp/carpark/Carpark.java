package bcccp.carpark;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.season.ISeasonTicket;
import bcccp.tickets.season.ISeasonTicketDAO;

public class Carpark implements ICarpark {
	
	private List<ICarparkObserver> observers;
	private String carparkId;
	private int capacity;
	private int numberOfCarsParked;
	private IAdhocTicketDAO adhocTicketDAO;
	private ISeasonTicketDAO seasonTicketDAO;
	
	
	
	public Carpark(String name, int capacity, 
			IAdhocTicketDAO adhocTicketDAO, 
			ISeasonTicketDAO seasonTicketDAO) {
		this.carparkId = name;
		this.capacity = capacity;
		this.numberOfCarsParked = 0;
		this.adhocTicketDAO = adhocTicketDAO;
		this.seasonTicketDAO = seasonTicketDAO;
		this.observers = new ArrayList<ICarparkObserver>();
	}



	@Override
	public void register(ICarparkObserver observer) {
		if (!this.observers.contains(observer))
		{
			this.observers.add(observer);
		}
	}

	@Override
	public void deregister(ICarparkObserver observer) {
		if (this.observers.contains(observer)) {
			this.observers.remove(observer);
		}
	}
	
	private void notifyObservers() {
		for (ICarparkObserver observer : observers) {
			observer.notifyCarparkEvent();
		}
	}
	
	public List<ICarparkObserver> getObservers(){
		return observers;
	}

	@Override
	public String getName() {
		return this.carparkId;
	}
	
	@Override
	public int getNumberOfCarsParked() {
		return this.numberOfCarsParked;
	}

	@Override
	public boolean isFull() {
		return numberOfCarsParked + seasonTicketDAO.getNumberOfTickets() == capacity;
	}

	@Override
	public IAdhocTicket issueAdhocTicket() throws Exception {
			return this.adhocTicketDAO.createTicket(this.carparkId);
		
	}

	@Override
	public IAdhocTicket getAdhocTicket(String barcode) {
		return this.adhocTicketDAO.findTicketByBarcode(barcode);
	}
	
	@Override
	public float calculateAddHocTicketCharge(long entryDateTime) {
		//TODO Implement charge logic
		
		float charge = 0.0f;
		long currentTime = System.currentTimeMillis();
		long diff =  currentTime - entryDateTime;
		
		long diffHours = diff / (60 * 60 * 1000);
		int i = 0;
		System.out.println(entryDateTime);
		System.out.println(diffHours);
	
		Calendar calEntry = Calendar.getInstance();
		calEntry.setTimeInMillis(entryDateTime);
		do{
			if(calEntry.get(Calendar.DAY_OF_WEEK)>1 && calEntry.get(Calendar.DAY_OF_WEEK)<7){
				if(calEntry.get(Calendar.HOUR_OF_DAY) > 6 && calEntry.get(Calendar.HOUR_OF_DAY)<20){
					charge+=5.0f;
				}else{
					charge+=2.0f;
				}
				
			}else{
				charge+=2.0f;
			}
			calEntry.add(Calendar.HOUR_OF_DAY, 1);
			i++;
				
		}while(i<diffHours);
		
		return charge;
		
		
	}

	@Override
	public boolean isSeasonTicketValid(String barcode) {		
		//ISeasonTicket ticket = seasonTicketDAO.findTicketById(barcode);		
		// TODO implement full validation logic
		//return ticket != null;
		ISeasonTicket ticket = this.seasonTicketDAO.findTicketById(barcode);
		Date date = new Date();
		if (ticket.getEndValidPeriod() >= date.getTime()) {
			return false;
		}
		if (ticket.getStartValidPeriod() <= date.getTime()) {
			return false;
		}
		return true;
	}

	@Override
	public void registerSeasonTicket(ISeasonTicket seasonTicket) {
		this.seasonTicketDAO.registerTicket(seasonTicket);		
	}

	@Override
	public void deregisterSeasonTicket(ISeasonTicket seasonTicket) {
		this.seasonTicketDAO.deregisterTicket(seasonTicket);
	
	}

	@Override
	public void recordSeasonTicketEntry(String ticketId) {
		ISeasonTicket ticket = seasonTicketDAO.findTicketById(ticketId);
		if (ticket == null) throw new RuntimeException("recordSeasonTicketEntry: invalid ticketId - " + ticketId);
		
		seasonTicketDAO.recordTicketEntry(ticketId);
		log(ticket.toString());
		//this.seasonTicketDAO.recordTicketEntry(ticketId);		
	}	
	
	private void log(String message) {
		System.out.println("Carpark : " + message);
	}

	@Override
	public void recordAdhocTicketEntry() {
		this.numberOfCarsParked++;
	}

	@Override
	public void recordAdhocTicketExit() {
		this.numberOfCarsParked--;
		notifyObservers();		
	}

	@Override
	public void recordSeasonTicketExit(String ticketId) {
		ISeasonTicket ticket = seasonTicketDAO.findTicketById(ticketId);
		if (ticket == null) throw new RuntimeException("recordSeasonTicketExit: invalid ticketId - " + ticketId);
		
		seasonTicketDAO.recordTicketExit(ticketId);
		log(ticket.toString());
		//this.seasonTicketDAO.recordTicketExit(ticketId);
	}

	@Override
	public boolean isSeasonTicketInUse(String ticketId) {
		ISeasonTicket ticket = seasonTicketDAO.findTicketById(ticketId);
		if (ticket == null) throw new RuntimeException("recordSeasonTicketExit: invalid ticketId - " + ticketId);
		
		return ticket.inUse();
		//ISeasonTicket ticket = this.seasonTicketDAO.findTicketById(ticketId);
		//return ticket.inUse();
	}




	public ISeasonTicketDAO getSeasonTicketDAO() {
		return seasonTicketDAO;
	}



	public int getCapacity() {
		return this.capacity;
	}



	@Override
	public IAdhocTicketDAO getAdhocTicketDAO() {
		return adhocTicketDAO;
	}

}

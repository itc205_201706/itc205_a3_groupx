package bcccp.tickets.adhoc;

import java.util.Date;

public class AdhocTicket implements IAdhocTicket {
	
	private String carparkId_;
	private int ticketNo_;
	private long entryDateTime;
	private long paidDateTime;
	private long exitDateTime;
	private float charge;
	private String barcode;
	private STATE state_;
	
	private enum STATE { ISSUED, CURRENT, PAID, EXITED }
	
	public AdhocTicket(String carparkId, int ticketNo, String barcode) throws Exception {
		if(carparkId.isEmpty()) throw new Exception("carparkId cannot be empty");
		this.carparkId_ = carparkId;
		if(ticketNo<=0) throw new Exception("ticketNo cannot be equal to or Less than 0");
		this.ticketNo_ = ticketNo;
		if(barcode.isEmpty()) throw new Exception("barcode cannot be null");
		this.barcode = barcode;
		this.state_ = STATE.ISSUED;
		
	}
	
	@Override
	public int getTicketNo() {
		return this.ticketNo_;
	}

	@Override
	public String getBarcode() {
		return this.barcode;
	}


	@Override
	public String getCarparkId() {
		return this.carparkId_;
	}

	@Override
	public boolean isIssued(){
		return state_ == STATE.ISSUED;
	}
	
	
	@Override
	public void enter(long entryDateTime) {
		this.entryDateTime = entryDateTime;
		this.state_ = STATE.CURRENT;		
	}


	@Override
	public long getEntryDateTime() {
		return this.entryDateTime;
	}

	@Override
	public void pay(long paidDateTime, float charge) {
		this.paidDateTime = paidDateTime;
		this.charge = charge;
		state_ = STATE.PAID;
	}


	@Override
	public long getPaidDateTime() {
		return this.paidDateTime;
	}


	@Override
	public float getCharge() {
		return this.charge;
	}



	public String toString() {
		Date entryDate = new Date(entryDateTime);
		Date paidDate = new Date(paidDateTime);
		Date exitDate = new Date(exitDateTime);
		return "Carpark    : " + carparkId_ + "\n" +
		       "Ticket No  : " + ticketNo_ + "\n" +
		       "Entry Time : " + entryDate + "\n" + 
		       "Paid Time  : " + paidDate + "\n" + 
		       "Exit Time  : " + exitDate + "\n" +
		       "State      : " + state_ + "\n" +
		       "Barcode    : " + barcode;		
	}


	@Override
	public boolean isCurrent() {
		return state_ == STATE.CURRENT;
	}

	@Override
	public boolean isPaid() {
		//if (this.paidDateTime > 0) {
		//	return true;
		//}
		//else return false;
		return state_ == STATE.PAID;
	}

	@Override
	public void exit(long dateTime) {
		this.exitDateTime = dateTime;	
		state_ = STATE.EXITED;
	}


	@Override
	public long getExitDateTime() {
		return this.exitDateTime;
	}


	@Override
	public boolean hasExited() {
		//if (this.exitDateTime > 0) {
		//	return true;
		//}
		//else return false;
		return state_ == STATE.EXITED;
	}


}
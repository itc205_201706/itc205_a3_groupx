package bcccp.tickets.adhoc;

public class AdhocTicketFactory implements IAdhocTicketFactory {

	@Override
	public IAdhocTicket make(String carparkId, int ticketNo) throws Exception {
		//String barcode = carparkId + Integer.toString(ticketNo);
		String barcode = "A" + Integer.toHexString(ticketNo);// + Long.toHexString(System.currentTimeMillis());
		return new AdhocTicket(carparkId, ticketNo, barcode);
	}

}
package bcccp.tickets.adhoc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class AdhocTicketDAO  implements IAdhocTicketDAO  {

	private Map<String, IAdhocTicket> currentTickets;
	private IAdhocTicketFactory adhocTicketFactory_;
	private int currentTicketNo;

	
	
	public AdhocTicketDAO(Map<String, IAdhocTicket> currentTickets, IAdhocTicketFactory adhocTicketFactory) {
		if(adhocTicketFactory.equals(null) || adhocTicketFactory == null) throw new RuntimeException("Factory cannot be null");
		this.adhocTicketFactory_ = adhocTicketFactory;
		this.currentTickets = currentTickets;		
	}



	@Override
	public IAdhocTicket createTicket(String carparkId) throws Exception {
		IAdhocTicket ticket = this.adhocTicketFactory_.make(carparkId, ++currentTicketNo);
		currentTickets.put(ticket.getBarcode(), ticket);
		return ticket;	
	}

	@Override
	public IAdhocTicket findTicketByBarcode(String barcode) {
		if(barcode.isEmpty() || barcode == null) throw new RuntimeException("barcode is null");
		return currentTickets.get(barcode);
	}	



	@Override
	public List<IAdhocTicket> getCurrentTickets() {		
		return Collections.unmodifiableList(new ArrayList<IAdhocTicket>(currentTickets.values()));
	}

}
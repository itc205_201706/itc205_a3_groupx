package bcccp.tickets.adhoc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class IntegrationTestAdhocTicketDAO {
	
	IAdhocTicketDAO sut;
	IAdhocTicketFactory ticketFactory;
	Map<String, IAdhocTicket> currentTickets;
	IAdhocTicket ticket;
	String carparkId;
	int currentTicketNo;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		ticketFactory = new AdhocTicketFactory();
		currentTickets = spy(new HashMap<String, IAdhocTicket>());
		carparkId = "testCarpark";
		currentTicketNo = 1;
		ticket = ticketFactory.make(carparkId, currentTicketNo);
		
		sut = new AdhocTicketDAO(currentTickets, ticketFactory);
	}

	@After
	public void tearDown() throws Exception {
		sut = null;
	}
	
	@Test(expected=RuntimeException.class) 
	public void testConstructorEmptyFactory() {
		sut = new AdhocTicketDAO(currentTickets, null);	
		fail("Should bave thrown exception");
	}

	@Test
	public void testCreateTicket() throws Exception {
		//setup
		
		//exec
		IAdhocTicket actual = sut.createTicket(carparkId);
		
		//verifies and asserts
		assertEquals(1,currentTickets.size());
		assertEquals(actual,currentTickets.get(actual.getBarcode()));
	}
	
	@Test(expected=RuntimeException.class) 
	public void testCreateTicketEmptyCarpark() throws Exception {
		IAdhocTicket actual = sut.createTicket(null);
		fail("Should bave thrown exception");
	}

	@Test
	public void testFindTicketByBarcode() throws Exception {
		//setup
		IAdhocTicket created = sut.createTicket(carparkId);
		
		//exec
		IAdhocTicket actual = sut.findTicketByBarcode(ticket.getBarcode());
		
		//verifies and asserts
		verify(currentTickets).get(eq(ticket.getBarcode()));
		assertEquals(created,actual);
	}
	
	@Test
	public void testFindTicketByBarcodeNotFound() throws Exception {
		//setup
		
		//exec
		IAdhocTicket actual = sut.findTicketByBarcode("A321");
		
		//verifies and asserts
		verify(currentTickets).get("A321");
		assertEquals(null,actual);
	}
	
	@Test(expected=RuntimeException.class) 
	public void testFindTicketByBarcodeEmptyBarcode() {
		IAdhocTicket actual = sut.findTicketByBarcode(null);
		fail("Should bave thrown exception");
	}

	@Test
	public void testGetCurrentTickets() throws Exception {
		//setup
		IAdhocTicket created = sut.createTicket(carparkId);
		
		//exec
		List<IAdhocTicket> allCurrentTickets = sut.getCurrentTickets();
		
		//verifies and asserts
		verify(currentTickets).values();
		assertEquals(created,allCurrentTickets.get(0));
	}

}

package bcccp.carpark.entry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.HashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bcccp.carpark.Carpark;
import bcccp.carpark.ICarSensor;
import bcccp.carpark.ICarpark;
import bcccp.carpark.IGate;
import bcccp.carpark.entry.EntryController.STATE;
import bcccp.tickets.adhoc.AdhocTicketDAO;
import bcccp.tickets.adhoc.AdhocTicketFactory;
import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.adhoc.IAdhocTicketFactory;
import bcccp.tickets.season.ISeasonTicketDAO;

public class IntegrationTestEntryController {
	
	EntryController sut;
	
	ICarpark carpark;
	ICarSensor outsideEntrySensor;
	ICarSensor insideEntrySensor;
	IEntryUI entryUI;
	IGate entryGate;
	IAdhocTicket adhoc;
	IAdhocTicketDAO adhocDAO;
	IAdhocTicketFactory adhocFactory;
	ISeasonTicketDAO seasonDAO;
	
	bcccp.carpark.entry.EntryController.STATE status;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		seasonDAO = mock(ISeasonTicketDAO.class);
		adhocFactory = spy(new AdhocTicketFactory());
		adhocDAO = spy(new AdhocTicketDAO(new HashMap<String, IAdhocTicket>(),adhocFactory));
		carpark = spy(new Carpark("test",2,adhocDAO,seasonDAO));
		outsideEntrySensor = mock(ICarSensor.class);
		insideEntrySensor = mock(ICarSensor.class);
		entryGate = mock(IGate.class);
		entryUI = mock(IEntryUI.class);
		
		sut = new EntryController(carpark, entryGate, 
				outsideEntrySensor, insideEntrySensor, entryUI);
	}

	@After
	public void tearDown() throws Exception {
		sut = null;
	}
	

	@Test
	public void testCarEventDetectedStateIdle() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.IDLE);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.WAITING,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateWaiting() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.WAITING);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateFull() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.FULL);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateIssued() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.ISSUED);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateValidated() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.VALIDATED);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateWaitingInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.WAITING);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.BLOCKED,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateFullInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.FULL);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.BLOCKED,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateIssuedInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.ISSUED);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.BLOCKED,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateValidatedInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.VALIDATED);
		
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.BLOCKED,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateBlocked() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.BLOCKED);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateBlockedInside() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.IDLE);
		sut.setState(STATE.BLOCKED);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateTaken() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.TAKEN);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateTakenInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = true;
		sut.setState(STATE.TAKEN);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.ENTERING,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateEntering() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.ENTERING);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.ENTERED,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateEnteringInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.ENTERING);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.TAKEN,sut.getState());
		verify(insideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateEntered() {
		//setup
		when(outsideEntrySensor.getId()).thenReturn("outsideEntrySensor");
		
		String detectorId = "outsideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.ENTERED);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.ENTERING,sut.getState());
		verify(outsideEntrySensor).getId();
		
	}
	
	@Test
	public void testCarEventDetectedStateEnteredInside() {
		//setup
		when(insideEntrySensor.getId()).thenReturn("insideEntrySensor");
		
		String detectorId = "insideEntrySensor";
		boolean carDetected = false;
		sut.setState(STATE.ENTERED);
		//exec
		sut.carEventDetected(detectorId, carDetected);
		
		//verifies and asserts
		assertEquals(STATE.IDLE,sut.getState());
		verify(entryGate).lower();
		verify(insideEntrySensor).getId();
		
	}

	@Test
	public void testButtonPushed() throws Exception {
		//setup
		doReturn(false).when(carpark).isFull();
		sut.setState(STATE.WAITING);
		
		//exec
		sut.buttonPushed();
		
		//verifies and asserts
		verify(carpark).isFull();
		verify(carpark).issueAdhocTicket();
		verify(entryUI).printTicket("test",1, 0L, "A1");
		
		assertEquals(STATE.ISSUED, sut.getState());
		assertTrue(carpark.getAdhocTicket("A1") instanceof IAdhocTicket);
		
	}
	
	@Test
	public void testButtonPushedFull() throws Exception {
		//setup
		//when(carpark.isFull()).thenReturn(true);
		doReturn(true).when(carpark).isFull();
		sut.setState(STATE.WAITING);
		
		//exec
		sut.buttonPushed();
		
		//verifies and asserts
		verify(carpark).isFull();
		verify(entryUI).display("CARPARK FULL");
		
		assertEquals(STATE.FULL, sut.getState());
		
	}
	
	@Test
	public void testButtonPushedOtherState() throws Exception {
		//setup
		doReturn(false).when(carpark).isFull();
		sut.setState(STATE.IDLE);
		
		//exec
		sut.buttonPushed();
		
		//verifies and asserts
		assertEquals(STATE.IDLE, sut.getState());
		verify(entryUI).beep();
		
	}

	@Test
	public void testTicketTakenStateIssued() {
		//setup
		sut.setState(STATE.ISSUED);
		
		//exec
		sut.ticketTaken();
		
		//verifies and asserts
		verify(entryUI).display("Ticket Taken");
		verify(entryGate).raise();
		assertEquals(STATE.TAKEN, sut.getState());
		
	}
	
	@Test
	public void testTicketTakenStateValidated() {
		//setup
		sut.setState(STATE.VALIDATED);
		
		//exec
		sut.ticketTaken();
		
		//verifies and asserts
		verify(entryUI).display("Ticket Taken");
		verify(entryGate).raise();
		assertEquals(STATE.TAKEN, sut.getState());
		
	}
	
	@Test
	public void testTicketTakenStateOther() {
		//setup
		sut.setState(STATE.IDLE);
		
		//exec
		sut.ticketTaken();
		
		//verifies and asserts
		verify(entryUI).beep();
		
	}

	@Test
	public void testNotifyCarparkEvent() {
		//setup
		sut.setState(STATE.FULL);
		doReturn(false).when(carpark).isFull();
//		when(carpark.isFull()).thenReturn(false);
		
		//exec
		sut.notifyCarparkEvent();
		
		//verifies and asserts		
		assertEquals(STATE.WAITING, sut.getState());
		
	}

}

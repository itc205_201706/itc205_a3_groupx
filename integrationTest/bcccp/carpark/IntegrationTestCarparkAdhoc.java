package bcccp.carpark;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bcccp.tickets.adhoc.AdhocTicketDAO;
import bcccp.tickets.adhoc.AdhocTicketFactory;
import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.adhoc.IAdhocTicketFactory;
import bcccp.tickets.season.ISeasonTicketDAO;

public class IntegrationTestCarparkAdhoc {
	
	Carpark sut;
	
	IAdhocTicketDAO adhocDao;
	ISeasonTicketDAO seasonDao;
	ICarparkObserver carparkObserver;
	
	String name;
	int capacity;
	int seasonTicketSpaces;
	
	
	@Before
	public void setUp(){
		name = "testName";
		capacity = 2;
		seasonTicketSpaces = 2;
		Map<String, IAdhocTicket> currentTickets = new HashMap<String, IAdhocTicket>();
		IAdhocTicketFactory ticketFactory = new AdhocTicketFactory();
		adhocDao = new AdhocTicketDAO(currentTickets,ticketFactory);
		seasonDao = mock(ISeasonTicketDAO.class);
		carparkObserver = mock(ICarparkObserver.class);
		
		sut = new Carpark(name,capacity,adhocDao,seasonDao);
		
	}
	
	@After
	public void breakDown(){
		sut = null;
	}
	
	
	/**
	 * Unique name for carpark
	 * Specify total capacity
	 * Specify capacity for season tickets
	 * 
	 */
	@Test
	public void testConstructor(){
		assertTrue(sut instanceof Carpark);
		assertTrue(sut.getName() instanceof String);
		assertTrue(Integer.class.isInstance(sut.getCapacity()) );
		assertTrue(sut.getAdhocTicketDAO() instanceof IAdhocTicketDAO);
		assertTrue(sut.getSeasonTicketDAO() instanceof ISeasonTicketDAO);
		
		assertEquals(sut.getName(), this.name);
		assertEquals(sut.getCapacity(), this.capacity);
		assertEquals(sut.getAdhocTicketDAO(), this.adhocDao);
		assertEquals(sut.getSeasonTicketDAO(), this.seasonDao);
	
	}
	
	@Test
	public void testRegister(){
		//setup
		
		//exec
		sut.register(carparkObserver);		
		
		//verifies and asserts
		List<ICarparkObserver> rtnList = sut.getObservers();
		assertEquals(rtnList.get(0), carparkObserver);
		
	}
	
	@Test
	public void testIssueAdhocTicket() throws Exception{
		//setup
		
		
		//exec
		sut.issueAdhocTicket();		
		
		//verifies and asserts
		if(sut.getCapacity() <= sut.getNumberOfCarsParked()){
			fail("more cars than capacity");
		}
		
		
	}
	
	@Test(expected=RuntimeException.class) 
	public void testIssueAdhocTicketMoreThanCapacity() throws Exception{
		//setup
		sut.recordAdhocTicketEntry();
		sut.recordAdhocTicketEntry();
		
		//exec
		sut.issueAdhocTicket();
		
		//verifies and asserts
	}
	
	@Test
	public void testRecordAdhocTicket(){
		//setup
		int numberBefore = sut.getNumberOfCarsParked();
		
		//exec
		sut.recordAdhocTicketEntry();		
		
		//verifies and asserts
		int numberAfter = sut.getNumberOfCarsParked();
		assertEquals(++numberBefore, numberAfter);
	}
	
	@Test
	public void testGetAdhocTicket(){
		//setup
		
		//exec
		sut.register(carparkObserver);		
		
		//verifies and asserts
		List<ICarparkObserver> rtnList = sut.getObservers();
		assertEquals(rtnList.get(0), carparkObserver);
		
	}
	
	
	
	
	
	@Test
	public void testCalculateAdhocTicketCharge(){
		
	}
	
	@Test
	public void testRecordAdhocTicketExit(){
		
	}
	

}
